// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameLastGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAMELAST_API ASnakeGameLastGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
