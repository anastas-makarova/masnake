// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeGameLast.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeGameLast, "SnakeGameLast" );
