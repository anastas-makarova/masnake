// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAMELAST_SnakeGameLastGameModeBase_generated_h
#error "SnakeGameLastGameModeBase.generated.h already included, missing '#pragma once' in SnakeGameLastGameModeBase.h"
#endif
#define SNAKEGAMELAST_SnakeGameLastGameModeBase_generated_h

#define SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_SPARSE_DATA
#define SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_RPC_WRAPPERS
#define SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeGameLastGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGameLastGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGameLastGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGameLast"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGameLastGameModeBase)


#define SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeGameLastGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGameLastGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGameLastGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGameLast"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGameLastGameModeBase)


#define SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGameLastGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGameLastGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGameLastGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGameLastGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGameLastGameModeBase(ASnakeGameLastGameModeBase&&); \
	NO_API ASnakeGameLastGameModeBase(const ASnakeGameLastGameModeBase&); \
public:


#define SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGameLastGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGameLastGameModeBase(ASnakeGameLastGameModeBase&&); \
	NO_API ASnakeGameLastGameModeBase(const ASnakeGameLastGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGameLastGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGameLastGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGameLastGameModeBase)


#define SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_12_PROLOG
#define SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_SPARSE_DATA \
	SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_RPC_WRAPPERS \
	SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_INCLASS \
	SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_SPARSE_DATA \
	SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAMELAST_API UClass* StaticClass<class ASnakeGameLastGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGameLast_Source_SnakeGameLast_SnakeGameLastGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
